#include "headers.h"

using namespace OC;
using namespace std;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;

static ObserveType OBSERVE_TYPE_TO_USE = ObserveType::Observe;


void t_resource::onGet(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode)
{
        cout << "In onGet callback" << endl;
        try
        {
                if(eCode == OC_STACK_OK) {

                        std::cout <<" Resource Uri ResourceUriHead_onGet getUri :"<< rep.getUri() << std::endl;
                        // hashredis.Writeredisget( this->key, rep);
                        this->Attdetals=rep.getPayload()->values;
                }

                else {
                        std::cout << "onGET Response error: " << eCode << std::endl;
                }
        }
        catch(std::exception& e)
        {
                std::cout << "Exception: " << e.what() << " in onGet" << std::endl;
        }
}

void t_resource::getRepresentation( )
{
        std::cout << "Getting Representation..."<<std::endl;
        // Invoke resource's get API with the callback parameter

        QueryParamsMap test;
        this->resource->get(test, std::bind( &t_resource::onGet,this,_1,_2,_3));
}

void t_resource::onPost(const HeaderOptions& /*headerOptions*/, const OCRepresentation& rep, const int eCode)
{
        try
        {
                if (eCode == OC_STACK_OK || eCode == OC_STACK_RESOURCE_CHANGED)
                {
                        // hashredis.Writeredisget( this->key, rep);
                        std::cout << "onPOST Response done: " << eCode << std::endl;
                }
                else
                {
                        std::cout << "onPOST Response error: " << eCode << std::endl;
                        std::exit(-1);
                }
        }

        catch(std::exception& e)
        {
                std::cout << "Exception: " << e.what() << " in onPost" << std::endl;
        }
}

void t_resource::postRepresentation(OCRepresentation rep )
{
        std::cout << "posting Representation..."<<std::endl;
        // Invoke resource's get API with the callback parameter
        this->resource->post(rep, QueryParamsMap(), std::bind( &t_resource::onPost,this,_1,_2,_3) );

}

void t_resource::onObserve(const HeaderOptions /*headerOptions*/, const OCRepresentation& rep,
                           const int& eCode, const int& sequenceNumber)
{
        try
        {
                if(eCode == OC_STACK_OK && sequenceNumber <= MAX_SEQUENCE_NUMBER)
                {
                        if(sequenceNumber == OC_OBSERVE_REGISTER)
                        {
                                std::cout << "Observe registration action is successful" << std::endl;
                        }


                        std::cout << "Resource URI: "<<rep.getUri()<<std::endl;

                        std::cout << "Observe action happend" << std::endl;

                        // representation(rep);
                }
                else
                {
                        if(eCode == OC_STACK_OK)
                        {
                                std::cout << "No observe option header is returned in the response." << std::endl;
                                std::cout << "For a registration request, it means the registration failed"
                                          << std::endl;
                        }
                        else
                        {
                                std::cout << "onObserve Response error: " << eCode << std::endl;
                                std::exit(-1);
                        }
                }
        }
        catch(std::exception& e)
        {
                std::cout << "Exception: " << e.what() << " in onObserve" << std::endl;
        }


}

void t_resource::observeRepresentation( )
{
        std::cout << "observing Representation..."<<std::endl;
        // Invoke resource's get API with the callback parameter
        this->resource->observe(OBSERVE_TYPE_TO_USE, QueryParamsMap(),
                                std::bind( &t_resource::onObserve,this,_1,_2,_3,_4) );
}
